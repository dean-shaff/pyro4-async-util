import os

cur_dir = os.path.dirname(os.path.abspath(__file__))
base_dir = os.path.dirname(cur_dir)
examples_dir = os.path.join(base_dir, "examples")


def main(input_file_path, output_file_path):
    file_names_dict = {
        "examples_basic_server": os.path.join(examples_dir, "basic", "server.py"),
        "examples_basic_client": os.path.join(examples_dir, "basic", "client.py"),
        "examples_event_emitter_server": os.path.join(examples_dir, "event_emitter", "server.py"),
        "examples_event_emitter_client": os.path.join(examples_dir, "event_emitter", "client.py")
    }

    file_names_contents = {}

    for key in file_names_dict:
        with open(file_names_dict[key], "r") as fd:
            contents = fd.read()
            file_names_contents[key] = contents

    with open(input_file_path, "r") as fd:
        contents = fd.read()

    contents = contents.format(**file_names_contents)

    with open(output_file_path, "w") as fd:
        fd.write(contents)


if __name__ == "__main__":
    input_file_path = os.path.join(base_dir, "README.template.md")
    output_file_path = os.path.join(base_dir, "README.md")
    main(input_file_path, output_file_path)
