import logging

import Pyro4

from .callback_proxy import CallbackProxy


module_logger = logging.getLogger(__name__)

__all__ = [
    "EventEmitter",
    "Pyro4EventEmitter",
    "Pyro4EventEmitterProxy"
]


class EventEmitter(object):

    def __init__(self, threaded=False):
        self._events = {}
        self._threaded = threaded

    @property
    def events(self):
        return self._events

    @property
    def threaded(self):
        return self._threaded

    def emit(self, event_name, *args, **kwargs):

        if event_name in self._events:
            for callback in self._events[event_name]:
                callback(*args, **kwargs)
        else:
            raise KeyError(
                ("EventEmitter.emit: "
                 "Can't find {} in _events").format(event_name))

    def on(self, event_name, func):
        if event_name not in self._events:
            self._events[event_name] = [func]
            return

        existing = False
        for callback in self._events[event_name]:
            if callback == func:
                existing = True
                break
        if not existing:
            self._events[event_name].append(func)

    def clear_all(self, event_name):
        """
        clear all the handles for a given ``event_name``

        Args:
            event_name (str): the name of the registered event
        Returns:
            None
        """
        module_logger.debug("EventEmitter.clear_all: {}".format(event_name))
        if event_name in self:
            self._events[event_name] = []

    def clear(self, event_name, func):
        """
        clear a given handle from a given ``event_name``

        Args:
            event_name (str): the name of the registered event
            func (callable): the handle to remove
        Returns:
            None
        """
        module_logger.debug(
            "EventEmitter.clear: {}, {}".format(event_name, func))
        if event_name in self:
            remove_idx = -1
            for idx, handle in enumerate(self._events[event_name]):
                if handle == func:
                    remove_idx = idx
                    break
            if remove_idx != -1:
                self._events[event_name].pop(remove_idx)

    def __contains__(self, item):
        return item in self._events


class Pyro4EventEmitter(EventEmitter):

    # @Pyro4.oneway
    @Pyro4.expose
    def on(self, event_name, func):
        module_logger.debug(
            "Pyro4EventEmitter.on: {}, {}".format(event_name, func))
        if not callable(func):
            func = getattr(func["handler"], func["name"])
        super(Pyro4EventEmitter, self).on(event_name, func)

    # @Pyro4.oneway
    @Pyro4.expose
    def emit(self, event_name, *args, **kwargs):
        module_logger.debug(
            "Pyro4EventEmitter.emit: {}, {}, {}".format(
                event_name, args, kwargs))

        if event_name in self._events:
            module_logger.debug(
                ("Pyro4EventEmitter.emit: emitting on {} events").format(
                    len(self._events[event_name])))
            remove_idx = []
            for idx, callback in enumerate(self._events[event_name]):
                module_logger.debug(
                    "Pyro4EventEmitter.emit: callback={}".format(callback))
                try:
                    callback(*args, **kwargs)
                except Pyro4.errors.ConnectionClosedError as err:
                    module_logger.error(
                        "Pyro4EventEmitter.emit: {}".format(err))
                    remove_idx.append(idx)
            for idx in remove_idx:
                self._events[event_name].pop(idx)

        else:
            raise KeyError(
                ("EventEmitter.emit: "
                 "Can't find {} in _events").format(event_name))


class Pyro4EventEmitterProxy(CallbackProxy):

    def on(self, event_name, func):
        registered_dict = self._register_callback(func)
        return Pyro4.Proxy._pyroInvoke(
            self, "on", (event_name, registered_dict), {})

    # def emit(self, event_name, *args, **kwargs):
    #     return Pyro4.Proxy._pyroInvoke(
    #         self, "emit", )
