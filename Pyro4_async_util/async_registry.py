import logging

__all__ = [
    "registry"
]

module_logger = logging.getLogger(__name__)


class Registry(object):

    registry_dict = {}

    def register(self, entry_name, entry, force=False):

        if entry_name in self.__class__.registry_dict:
            if not force:
                return

        self.__class__.registry_dict[entry_name] = entry

    def unregister(self, entry_name):
        if entry_name in self.__class__.registry_dict:
            del self.__class__.registry_dict[entry_name]

    def __contains__(self, item):
        return item in self.__class__.registry_dict


registry = Registry()


class CallbackHelper(object):
    """
    Abstract Base Class for Callback helpers. Any derived classes need to
    implement these methods.
    """
    @classmethod
    def get_cb(cls, kwargs=None):
        """
        Returns:
            tuple: (callback, new_kwargs), where callback is the callback
                object that will get called in the async_method decorated
                server method, and new_kwargs get passed onto the server
                method.
        """
        raise NotImplementedError

    @classmethod
    def set_cb(cls, proxy, kwargs=None):
        """
        Returns:
            dict: new keyword arguments that get passed to the proxy that
                dispatches calls to the remote.
        """
        raise NotImplementedError


class Pyro4DefaultCallbackHelper(CallbackHelper):
    """
    This is the CallbackHelper that helps implement truly asynchronous
    callbacks in Pyro4.
    """
    name = "pyro4_callback"

    @classmethod
    def get_cb(cls, kwargs=None):

        callback_name = kwargs.pop("_cb_name", None)
        pyro_handler = kwargs.pop("_cb_handler", None)

        if callback_name and pyro_handler:
            callback = getattr(pyro_handler, callback_name)
            return callback, kwargs
        else:
            return None, kwargs

    @classmethod
    def set_cb(cls, proxy, kwargs=None):
        if kwargs is None:
            return {}
        else:
            cb = kwargs.pop("cb", None)
            if cb is not None:
                registered_cb = proxy._register_callback(cb)
                kwargs["_cb_name"] = registered_cb["name"]
                kwargs["_cb_handler"] = registered_cb["handler"]
                kwargs["_use"] = cls.name

            return kwargs


registry.register(Pyro4DefaultCallbackHelper.name, Pyro4DefaultCallbackHelper)
