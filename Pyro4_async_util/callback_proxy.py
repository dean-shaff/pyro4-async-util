import logging
import threading
import inspect

import six
import Pyro4.core

from .async_registry import registry


module_logger = logging.getLogger(__name__)


class CallbackProxy(Pyro4.core.Proxy):

    __asyncUtilAttributes = frozenset(
        ["_inner_daemon",
         "_inner_daemon_thread",
         "_is_closed_event",
         "_registered_callback_functions",
         "_register_callback",
         "_create_handler_cls"])

    def __init__(self, *args, **kwargs):
        """
        By passing an existing daemon instance, the CallbackProxy assumes
        that you will handle the event loop of the daemon yourself. Otherwise,
        the CallbackProxy will create its own thread to run the daemon's
        ``requestLoop`` function.

        You can also pass a thread class to use instead of the stdlib
        threading.Thread class. If you do, it must implement the same interface
        as the stdlib threading.Thread class:

        ... code-block::python

            t = thread_cls(target=func)
            t.daemon = True
            t.start()
            ...
            t.join()

        """
        existing_daemon = kwargs.pop("_existing_daemon", None)
        thread_cls = kwargs.pop("_thread_cls", threading.Thread)
        module_logger.debug(("CallbackProxy.__init__: "
                             "existing_daemon={}").format(existing_daemon))
        module_logger.debug(("CallbackProxy.__init__: "
                             "thread_cls={}").format(thread_cls))

        self._inner_daemon_thread = None
        self._registered_callback_functions = []

        if existing_daemon is None:
            self._inner_daemon = Pyro4.Daemon()
            self._inner_daemon_thread = thread_cls(
                target=self._inner_daemon.requestLoop)
            self._inner_daemon_thread.daemon = True
            self._inner_daemon_thread.start()
        else:
            self._inner_daemon = existing_daemon

        super(CallbackProxy, self).__init__(*args, **kwargs)

    def __getattr__(self, name):
        # module_logger.debug("CallbackProxy.__getattr__: {}".format(name))
        if name in CallbackProxy.__asyncUtilAttributes:
            return self.__getattribute__(name)
        return super(CallbackProxy, self).__getattr__(name)

    def __setattr__(self, name, value):
        # module_logger.debug("CallbackProxy.__setattr__: {}".format(name))
        if name in CallbackProxy.__asyncUtilAttributes:
            return object.__setattr__(self, name, value)
        else:
            return super(CallbackProxy, self).__setattr__(name, value)

    def _pyroInvoke(self, methodname, vargs, kwargs, **kw):

        if kwargs is None:
            kwargs = {}

        use = kwargs.get("_use", "pyro4_callback")
        if use not in registry:
            raise KeyError("Can't find {} in callback registry".format(use))

        entry = registry.registry_dict[use]

        new_kwargs = entry.set_cb(self, kwargs)

        return super(CallbackProxy, self)._pyroInvoke(
            methodname, vargs, new_kwargs, **kw)

    def _register_callback(self, callback):

        def _register_on_daemon(obj):
            if hasattr(obj, "_pyroId"):
                uri = self._inner_daemon.uriFor(obj)
            else:
                uri = self._inner_daemon.register(obj)
            return uri

        module_logger.debug(
            "CallbackProxy.register_callback: callback={}".format(callback))
        if inspect.ismethod(callback):
            module_logger.debug(
                "CallbackProxy.register_callback: registering method")
            obj = six.get_method_self(callback)
            uri = _register_on_daemon(obj)
            module_logger.debug(
                "CallbackProxy.register_callback: uri={}".format(uri))
            return {
                "name": callback.__name__,
                "handler": Pyro4.Proxy(uri)
            }
        elif inspect.isfunction(callback):
            module_logger.debug(
                "CallbackProxy.register_callback: registering function")
            # check existing handlers for this callback function
            obj = None
            for _obj in self._registered_callback_functions:
                if callback is _obj.original_func[0]:
                    obj = _obj
                    break
            if obj is None:
                obj = self._create_handler_cls(callback)
                self._registered_callback_functions.append(obj)

            return self._register_callback(obj.callback)
        else:
            raise TypeError(
                "Currently not supporting type={}".format(type(callback)))

    def _create_handler_cls(self, func):

        class Handler(object):

            original_func = (func, )  # have to use a tuple, not an attribute

            @Pyro4.expose
            def callback(self, *args, **kwargs):
                return func(*args, **kwargs)

        Handler.callback.__name__ = func.__name__
        return Handler()

    def close(self):
        self._inner_daemon.shutdown()
