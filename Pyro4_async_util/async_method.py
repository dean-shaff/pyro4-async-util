import logging
import functools
import inspect

import Pyro4

from .async_registry import registry


__all__ = [
    "async_method"
]

module_logger = logging.getLogger(__name__)


def async_method(func):
    """
    Decorator for server side functions, indicating that they can be used
    in an asynchronous manner.

    Args:
        func (function): a method of a class.
    Returns:
        function: decorated method.
    """
    @functools.wraps(func)
    def _async_method(*args, **kwargs):
        if kwargs is None:
            kwargs = {}
        module_logger.debug(
            "async_method._async_method: kwargs={}".format(kwargs))

        use = kwargs.pop("_use", "pyro4_callback")
        if use not in registry:
            use = "pyro4_callback"

        entry = registry.registry_dict[use]

        callback, new_kwargs = entry.get_cb(kwargs=kwargs)
        _async_method.cb = callback

        if callback is not None:
            if inspect.isgeneratorfunction(func):
                for res in func(*args, **new_kwargs):
                    callback(res)
            else:
                callback(func(*args, **new_kwargs))

    return Pyro4.expose(Pyro4.oneway(_async_method))
