import functools


def async_callback(func):

    func._pyroExposed = True

    @functools.wraps(func)
    def _async_callback(*args, **kwargs):

        return func(*args, **kwargs)

    return _async_callback
