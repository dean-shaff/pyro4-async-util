## Pyro4 Asynchronous Utilities

HTTP like server and client utilities for Pyro4

### Installation:

With [poetry](https://python-poetry.org/) installed:

```
poetry install
```

Without poetry:

```
pip install -r requirements.txt
```

### Usage:

#### Callback based server and client

server side:

```python
import time

import Pyro4
import Pyro4_async_util as async_util


class MyServer:

    @async_util.async_method
    def long_running_method(self, *args, **kwargs):
        time.sleep(1.0)
        return "long_running_method"

    @async_util.async_method
    def long_running_method_with_updates(self, *args, **kwargs):
        for idx in range(3):
            yield f"update {idx}"
            time.sleep(1.0)

if __name__ == "__main__":
    server = MyServer()
    with Pyro4.Daemon(port=50001) as daemon:
        uri = daemon.register(server, objectId="basic")
        print(f"Running with uri {uri}")
        daemon.requestLoop()

```


client side:

```python
import Pyro4_async_util as async_util

uri = "PYRO:basic@localhost:50001"
proxy = async_util.CallbackProxy(uri)

def callback(res):
    print(res)

proxy.long_running_method_with_updates(cb=callback)

while True:
    pass

```

#### Asynchronous event emitter


server side

```python
# server.py
import logging

import Pyro4
import Pyro4_async_util as async_util


class MyEventEmitter(async_util.Pyro4EventEmitter):

    @Pyro4.expose
    def method(self):
        print("MyEventEmitter.method")
        self.emit("event", "hello")


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    server = MyEventEmitter()
    with Pyro4.Daemon(port=50002) as daemon:
        uri = daemon.register(server, objectId="event_emitter")
        print(f"Running with uri {uri}")
        daemon.requestLoop()

```

client side


```python
import Pyro4_async_util as async_util


uri = "PYRO:event_emitter@localhost:50002"

proxy = async_util.Pyro4EventEmitterProxy(uri)

def callback(res):
    print(res)
    print("callback")


proxy.on("event", callback)
proxy.emit("event", "hello")
proxy.method()

while True:
    pass

```
