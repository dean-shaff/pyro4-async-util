import asyncio

import Pyro4.core

from .async_registry import registry, CallbackHelper
from .callback_proxy import CallbackProxy


class AsyncCallbackHelper(CallbackHelper):

    @classmethod
    def set_cb(cls, proxy, kwargs=None):
        pass

    @classmethod
    def get_cb(cls, kwargs):
        pass


registry.register("async_helper", AsyncCallbackHelper)


class AsyncProxy(CallbackProxy):

    async def _pyroInvoke(self, methodname, vargs, kwargs, **kw):

        loop = asyncio.get_running_loop()

        use = kwargs.get("_use", "async_helper")
        if use not in registry:
            raise KeyError("Can't find {} in callback registry".format(use))

        entry = registry.registry_dict[use]

        new_kwargs = entry.set_cb(self, kwargs)
        if new_kwargs == kwargs:
            return Pyro4.core.Proxy._pyroInvoke(
                self, methodname, vargs, kwargs, **kw)
        else:
            new_kwargs["_use"] = use
