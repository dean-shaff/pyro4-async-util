import asyncio
import threading
import functools

import Pyro4

import async_util


class TestServer(object):

    @async_util.async_method
    def method(self):
        return "hello"


server = TestServer()
daemon = Pyro4.Daemon()
uri = daemon.register(server)
daemon_thread = threading.Thread(target=daemon.requestLoop)
daemon_thread.daemon = True
daemon_thread.start()


async def main(loop):
    # Create a new Future object.
    fut = loop.create_future()
    fut.add_done_callback(
        functools.partial(print, "Future:"))

    class CallbackClass(object):

        def __init__(self, fut):
            self.fut = fut

        @Pyro4.expose
        async def callback(self, res):
            print(res)
            self.fut.set_result(res)
            print("here")


    proxy = async_util.CallbackProxy(uri)

    obj = CallbackClass(fut)

    proxy.method(cb=obj.callback)

    await fut

loop = asyncio.get_event_loop()
loop.run_until_complete(main(loop))
loop.close()
