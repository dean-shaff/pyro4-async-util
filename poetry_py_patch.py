"""
poetry v0.12.17 has a bug where we can't specify multiple versions of Python,
like in pyproject.backup.toml
"""

import sys

import toml

py_version = ">=3.5"

if len(sys.argv) > 1:
    py_version = sys.argv[1]

file_name = "pyproject.toml"

with open(file_name, "r") as f:
    contents = toml.load(f)

contents["tool"]["poetry"]["dependencies"]["python"] = py_version

with open(file_name, "w") as f:
    toml.dump(contents, f)
