## Pyro4 Asynchronous Utilities

HTTP like server and client utilities for Pyro4

### Installation:

With [poetry](https://python-poetry.org/) installed:

```
poetry install
```

Without poetry:

```
pip install -r requirements.txt
```

### Usage:

#### Callback based server and client

server side:

```python
{examples_basic_server}
```


client side:

```python
{examples_basic_client}
```

#### Asynchronous event emitter


server side

```python
{examples_event_emitter_server}
```

client side


```python
{examples_event_emitter_client}
```
