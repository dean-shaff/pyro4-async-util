# server.py
import logging

import Pyro4
import Pyro4_async_util as async_util


class MyEventEmitter(async_util.Pyro4EventEmitter):

    @Pyro4.expose
    def method(self):
        print("MyEventEmitter.method")
        self.emit("event", "hello")


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    server = MyEventEmitter()
    with Pyro4.Daemon(port=50002) as daemon:
        uri = daemon.register(server, objectId="event_emitter")
        print(f"Running with uri {uri}")
        daemon.requestLoop()
