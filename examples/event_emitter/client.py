import Pyro4_async_util as async_util


uri = "PYRO:event_emitter@localhost:50002"

proxy = async_util.Pyro4EventEmitterProxy(uri)

def callback(res):
    print(res)
    print("callback")


proxy.on("event", callback)
proxy.emit("event", "hello")
proxy.method()

while True:
    pass
