import Pyro4_async_util as async_util

uri = "PYRO:basic@localhost:50001"
proxy = async_util.CallbackProxy(uri)

def callback(res):
    print(res)

proxy.long_running_method_with_updates(cb=callback)

while True:
    pass
