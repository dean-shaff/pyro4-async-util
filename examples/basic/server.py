import time

import Pyro4
import Pyro4_async_util as async_util


class MyServer:

    @async_util.async_method
    def long_running_method(self, *args, **kwargs):
        time.sleep(1.0)
        return "long_running_method"

    @async_util.async_method
    def long_running_method_with_updates(self, *args, **kwargs):
        for idx in range(3):
            yield f"update {idx}"
            time.sleep(1.0)

if __name__ == "__main__":
    server = MyServer()
    with Pyro4.Daemon(port=50001) as daemon:
        uri = daemon.register(server, objectId="basic")
        print(f"Running with uri {uri}")
        daemon.requestLoop()
