import unittest
import logging

from Pyro4_async_util.async_method import async_method
from Pyro4_async_util.async_registry import (
    registry, Registry, CallbackHelper)


class TestAsyncRegistry(unittest.TestCase):
    """
    Test the basic methods of the registry
    """

    def setUp(self):
        self.registry = Registry()

    def test_register(self):
        self.registry.register("dummy", CallbackHelper)
        self.assertTrue("dummy" in self.registry)

    def test_unregister(self):
        self.registry.register("dummy", CallbackHelper)
        self.registry.unregister("dummy")
        self.assertFalse("dummy" in self.registry)


class CustomCallbackHelper(CallbackHelper):

    @classmethod
    def get_cb(cls, kwargs=None):
        callback = kwargs.pop("cb", None)
        return callback, kwargs

    @classmethod
    def set_cb(cls, proxy, kwargs=None):
        pass


class TestServer(object):

    @async_method
    def method(self):
        return "hello"


class TestCustomRegistry(unittest.TestCase):

    def test_custom_registry(self):

        called = {
            "callback": False
        }

        def callback(res):
            called["callback"] = True

        registry.register("custom", CustomCallbackHelper)
        server = TestServer()

        server.method(cb=callback, _use="custom")


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    unittest.main()
