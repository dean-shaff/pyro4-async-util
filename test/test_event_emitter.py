import unittest
import logging
import time

import Pyro4

import Pyro4_async_util.event_emitter as event_emitter

from .util import Pyro4TestCase_factory

Pyro4.config.SERVERTYPE = "multiplex"


class TestEventEmitterServer(event_emitter.Pyro4EventEmitter):

    def __init__(self, **kwargs):
        self.calls = {
            "callback_server_side": 0
        }
        super(TestEventEmitterServer, self).__init__(self, **kwargs)

    def callback_server_side(self, res):
        # print("callback_server_side: res={}".format(res))
        self.calls["callback_server_side"] += 1

    @Pyro4.expose
    def call_on_server_side(self):
        self.on("event", self.callback_server_side)

    @Pyro4.expose
    def call_emit_server_side(self):
        self.emit("event", "hello from server")


class Callback(object):

    def __init__(self):
        self.calls = {
            "__call__": 0,
            "handle": 0
        }

    def __call__(self, res):
        self.calls["__call__"] += 1

    @Pyro4.expose
    def handle(self, res):
        self.calls["handle"] += 1


class TestEventEmitter(unittest.TestCase):

    def setUp(self):
        self.emitter = event_emitter.EventEmitter()

    def test_on(self):
        callback = Callback()
        self.emitter.on("event", callback)
        self.assertTrue(callback in self.emitter._events["event"])

        self.emitter.on("event", callback)
        self.assertTrue(len(self.emitter._events["event"]) == 1)

    def test_emit(self):
        callback = Callback()
        self.emitter.on("event", callback)
        self.emitter.emit("event", "hello")
        self.assertTrue(callback.calls["__call__"] == 1)

        with self.assertRaises(KeyError):
            self.emitter.emit("event1")

    def test_clear_all(self):

        callback = Callback()
        self.emitter.on("event", callback)
        self.emitter.on("event", callback.handle)
        self.emitter.clear_all("event")

        self.assertTrue(len(self.emitter._events["event"]) == 0)

    def test_clear(self):
        def handle(res):
            pass

        callback = Callback()
        self.emitter.on("event", callback)
        self.emitter.on("event", callback.handle)
        self.emitter.on("event", handle)
        self.emitter.clear("event", callback.handle)
        self.emitter.clear("event", handle)

        self.assertTrue(len(self.emitter._events["event"]) == 1)
        self.assertTrue(self.emitter._events["event"][0] == callback)


class TestPyro4EventEmitter(unittest.TestCase):

    def setUp(self):
        self.server = TestEventEmitterServer()

    def test_on(self):
        self.server.call_on_server_side()
        self.assertTrue(
            self.server.callback_server_side in self.server._events["event"])

    def test_emit(self):
        self.server.call_on_server_side()
        self.server.call_emit_server_side()

        self.assertTrue(
            self.server.calls["callback_server_side"] == 1)


class TestPyro4EventEmitterProxy(
    Pyro4TestCase_factory(TestEventEmitterServer)
):

    def setUp(self):
        self.proxy = event_emitter.Pyro4EventEmitterProxy(self.uri)

    def test_on(self):

        self.server.clear_all("event")
        callback = Callback()
        self.proxy.on("event", callback.handle)
        self.assertTrue(len(self.server._events["event"]) == 1)
        self.proxy.call_on_server_side()
        self.assertTrue(len(self.server._events["event"]) == 2)

    def test_emit(self):

        self.server.clear_all("event")
        callback = Callback()
        self.proxy.on("event", callback.handle)
        self.proxy.emit("event", "hello from client")
        while callback.calls["handle"] == 0:
            pass
        self.assertTrue(callback.calls["handle"] == 1)

        self.proxy.call_on_server_side()
        self.proxy.emit("event", "hello from client")
        while callback.calls["handle"] == 0:
            pass
        self.assertTrue(callback.calls["handle"] == 2)
        self.assertTrue(self.server.calls["callback_server_side"] == 1)

        self.proxy.call_emit_server_side()
        self.assertTrue(callback.calls["handle"] == 3)
        self.assertTrue(self.server.calls["callback_server_side"] == 2)

    def test_emit_after_disconnect(self):

        self.server.clear_all("event")
        self.server.calls["callback_server_side"] = 0
        callback = Callback()
        self.proxy.on("event", callback.handle)
        self.proxy.call_on_server_side()

        self.proxy.close()
        self.proxy._inner_daemon_thread.join()
        while self.proxy._inner_daemon_thread.is_alive():
            time.sleep(0.1)

        self.server.emit("event", "hello")
        self.assertTrue(callback.calls["handle"] == 0)
        self.assertTrue(self.server.calls["callback_server_side"] == 1)
        self.server.emit("event", "hello")
        self.assertTrue(self.server.calls["callback_server_side"] == 2)


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    unittest.main()
