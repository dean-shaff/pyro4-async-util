import unittest
import logging

# import sys
# import os
#
# cur_dir = os.path.dirname(os.path.abspath(__file__))
# base_dir = os.path.dirname(cur_dir)
# pyro4_dir = os.path.join(os.path.dirname(base_dir), "Pyro4/src")
#
# sys.path.insert(0, pyro4_dir)

import Pyro4
import Pyro4_async_util as async_util

from .util import Pyro4TestCase_factory


class TestServer(object):

    @Pyro4.expose
    def method(self):
        return "method"

    @async_util.async_method
    def callback_method_no_args(self):
        return "hello"

    @async_util.async_method
    def callback_method(self, *args, **kwargs):
        return "hello"

    @async_util.async_method
    def callback_generator_method(self, *args, **kwargs):
        for c in "hello":
            yield c


class CallbackClass(object):

    def __init__(self):
        self.calls = {
            "callback": 0
        }
        self.called = {
            "callback": False
        }

    @Pyro4.expose
    def callback(self, res):
        self.calls["callback"] += 1
        self.called["callback"] = True


class TestCallbackProxy(Pyro4TestCase_factory(TestServer)):

    def setUp(self):
        self.proxy = async_util.CallbackProxy(self.__class__.uri)

    def test__register_callback_function(self):

        def callback(res):
            pass

        self.proxy._register_callback(callback)
        self.proxy._register_callback(callback)
        self.assertTrue(len(self.proxy._registered_callback_functions) == 1)

    def test__register_callback_method(self):

        obj = CallbackClass()
        res0 = self.proxy._register_callback(obj.callback)
        self.assertTrue("name" in res0)
        self.assertTrue("handler" in res0)
        # do it again
        res1 = self.proxy._register_callback(obj.callback)
        self.assertEqual(res0, res1)

    def test_call_remote_method_no_callback(self):

        self.proxy.callback_method()
        self.proxy.method()

    def test_call_remote_method(self):

        obj = CallbackClass()
        self.proxy.callback_method(cb=obj.callback)
        while not obj.called["callback"]:
            pass
        self.assertTrue(obj.calls["callback"] == 1)

    def test_call_remote_method_no_args(self):

        obj = CallbackClass()
        self.proxy.callback_method_no_args(cb=obj.callback)
        while not obj.called["callback"]:
            pass
        self.assertTrue(obj.calls["callback"] == 1)

    def test_call_remote_generator_method(self):

        obj = CallbackClass()
        self.proxy.callback_generator_method(cb=obj.callback)
        while obj.calls["callback"] < 5:
            pass
        self.assertTrue(obj.calls["callback"] == 5)


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    unittest.main()
