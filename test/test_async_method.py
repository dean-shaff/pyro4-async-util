import unittest
import logging

import Pyro4_async_util as async_util


class TestServer(object):

    @async_util.async_method
    def method(self, *args, **kwargs):
        pass


class TestAsyncMethod(unittest.TestCase):

    def test_async_method_decorator(self):

        server = TestServer()
        server.method()
        server.method()


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    unittest.main()
