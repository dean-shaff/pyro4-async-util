import logging
import unittest
import threading

import Pyro4


module_logger = logging.getLogger(__name__)


def Pyro4TestCase_factory(server_cls, *args, **kwargs):
    """
    Args:
        server_cls (cls): A class to be used as the Pyro4 server
        args (tuple): Passed to ``server_cls.__init__``
        kwargs (dict): Passed to ``server_cls.__init__``
    Returns:
        cls: class inheriting from unittest.TestCase
    """

    class Pyro4TestCase(unittest.TestCase):

        @classmethod
        def setUpClass(cls):
            cls.server = server_cls(*args, **kwargs)
            cls.daemon = Pyro4.Daemon()
            cls.uri = cls.daemon.register(cls.server)
            cls._daemon_thread = threading.Thread(
                target=cls.daemon.requestLoop)
            cls._daemon_thread.daemon = True
            cls._daemon_thread.start()
            module_logger.debug(
                ("Pyro4TestCase.setUpClass: uri={}").format(cls.uri))

        @classmethod
        def tearDownClass(cls):
            cls.daemon.shutdown()
            cls._daemon_thread.join()

    return Pyro4TestCase
